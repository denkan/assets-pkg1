# Asset Package 1

A asset package which include Grunt management for CSS/SASS and JS. Also FTP deployment.


## Getting Started
1) Open Terminal and locate this folder.
2) Run "npm install" to auto-install packages.
3) Ready to go! Try run "grunt" in same Terminal.

Tip: To watch files when developing, try these commands
grunt watch:js
grunt watch:css

Also deploy?
grunt watch:jsdeploy
grunt watch:csdeploy

Recommended to use separate Terminal tabs/windows for css and js watch.



## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
