'use strict';

module.exports = function(grunt) {
  var DEPLOY_MODE = 'DEV';


  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),

    // Task configuration.
    clean: {
      js: ['build/js'],
      css: ['build/css']
    },

    concat: {
      js: {
        src: grunt.file.expand('src/js/**/*.js', '!**/[modernizr,jquery]*.js'),
        dest: 'build/js/<%= pkg.name %>.js'
      }
    },
    uglify: {
      js: {
        src: '<%= concat.js.dest %>',
        dest: 'build/js/<%= pkg.name %>.min.js'
      }
    },
    sass: {
      all: {
        files: [{
          expand: true,
          cwd: 'src/sass',
          src: ['*.scss'],
          dest: 'build/css',
          ext: '.css'
        }]
      }
    },
    copy: {
      js: {
        files: [
          {
            expand: true,
            cwd: 'build/js',
            src: '*.js',
            dest: '../httpdocs/Scripts/'
          }
        ]
      },
      css: {
        files: [
          {
            expand: true,
            cwd: 'build/css',
            src: '*.css',
            dest: '../httpdocs/Css/'
          }
        ]
      }
    },
    ftpscript: { // Don't forget to set up your .ftppass file
      jsDEV: {
        options: {
          host: 'dev.denkan.se', 
          passive: false,
          mkdir: false,
          authKey:'key1'
        },
        files: [
          {
            expand: true,
            cwd: 'build/js',
            src: '<%= pkg.name %>.js',
            dest: '/HTTP/website.se/httpdocs/Scripts/'
          }
        ]
      },
      cssDEV: {
        options: {
          host: 'dev.denkan.se', 
          passive: false,
          mkdir: false,
          authKey:'key1'
        },
        files: [
          {
            expand: true,
            cwd: 'build/css',
            src: '*.css',
            dest: '/HTTP/website.se/httpdocs/Css/'
          }
        ]
      }

    },

    watch: {
      js: {
        files: '<%= concat.js.src %>',
        tasks: ['js']
      },
      jsdeploy: {
        files: '<%= concat.js.src %>',
        tasks: ['jsdeploy']
      },
      css: {
        files: 'src/sass/**/*',
        tasks: ['css']
      },
      cssdeploy: {
        files: 'src/sass/**/*',
        tasks: ['cssdeploy']
      }
       
    }

  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-ftpscript');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Default task.
  grunt.registerTask('default', ['js','css']);
  grunt.registerTask('js', ['clean:js', 'concat:js', 'uglify:js' /*,'copy:js'*/]);
  grunt.registerTask('jsdeploy', ['js','ftpscript:js'+DEPLOY_MODE]);
  grunt.registerTask('css', ['clean:css', 'sass' /*,'copy:css'*/]);
  grunt.registerTask('cssdeploy', ['css','ftpscript:css'+DEPLOY_MODE]);



};
